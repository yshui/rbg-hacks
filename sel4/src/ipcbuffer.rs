use crate::cap::{CPtr, CSlot};
use crate::generated::{seL4_IPCBuffer, seL4_MessageInfo_t, seL4_Word, seL4_CPtr};
use crate::Error;
use crate::addressspace::{AddressSpace, GS, ASAccess};
use core::mem::size_of;
pub struct IPCBuffer;

type ASGS = AddressSpace<GS>;

macro_rules! offset_of {
    ($container:path, $field:ident) => {{
        unsafe { &(*core::ptr::null::<$container>()).$field as *const _ as usize }
    }};
}
macro_rules! set_mrn {
    ($id:expr, $val:expr) => {{
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, msg) + $id * size_of::<seL4_Word>();
        ASGS::ptr(OFFSET).unwrap().write(&($val as seL4_Word));
    }};
}
#[cfg(target_arch = "x86_64")]
impl IPCBuffer {
    pub unsafe fn get_info() -> u64 {
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, tag);
        ASGS::ptr(OFFSET).unwrap().read()
    }

    pub unsafe fn set_info(info: u64) {
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, tag);
        ASGS::ptr(OFFSET).unwrap().write(&info);
    }

    pub unsafe fn set_caps(caps: &[CPtr]) -> Result<(), Error> {
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, caps_or_badges);
        const SIZE: usize = size_of::<seL4_CPtr>();
        ASGS::slice(OFFSET, 3).unwrap().write(caps)?;
        let old_info = Self::get_info();
        Self::set_info(old_info & !0x18u64 | (caps.len() as u64) << 7);
        Ok(())
    }
    pub unsafe fn set_tag(tag: u64) {
        let old_info = Self::get_info();
        Self::set_info(old_info & 0xfff | tag << 12);
    }
    pub unsafe fn set_self_pointer(data: *const IPCBuffer) {
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, userData);
        ASGS::ptr(OFFSET).unwrap().write(&data);
    }
    pub unsafe fn set_mr(mr1: seL4_Word, mr2: seL4_Word, mr3: seL4_Word, mr4: seL4_Word) {
        set_mrn!(0, mr1);
        set_mrn!(1, mr2);
        set_mrn!(2, mr3);
        set_mrn!(3, mr4);
    }
    pub unsafe fn set_msg(msg: &[seL4_Word]) -> Result<(), Error> {
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, msg);
        ASGS::slice(OFFSET, 120).unwrap().write(msg)?;
        let old_info = Self::get_info();
        Self::set_info(old_info & !0x7f | msg.len() as u64);
        Ok(())
    }
    pub unsafe fn get_msg(msg: &mut [seL4_Word]) -> Result<usize, Error> {
        let len = Self::get_info() & 0x7f;
        const OFFSET: usize = offset_of!(seL4_IPCBuffer, msg);
        ASGS::slice(OFFSET, len as usize).unwrap().read(msg)?;
        Ok(len as usize)
    }
    pub unsafe fn set_receive_cnode(root: CPtr, slot: CSlot) {
        const OFFSET_CNODE: usize = offset_of!(seL4_IPCBuffer, receiveCNode);
        const OFFSET_INDEX: usize = offset_of!(seL4_IPCBuffer, receiveIndex);
        const OFFSET_DEPTH: usize = offset_of!(seL4_IPCBuffer, receiveDepth);
        ASGS::ptr(OFFSET_CNODE).unwrap().write(&root.0);
        ASGS::ptr(OFFSET_INDEX).unwrap().write(&slot.cptr.0);
        ASGS::ptr(OFFSET_DEPTH).unwrap().write(&(slot.depth as seL4_Word));
    }
}
