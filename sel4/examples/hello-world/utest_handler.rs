// TODO: register alternative output sources (default to writeln!(sel4::Debug, ..) if
// KernelPrinting is on, else none)

// TODO: propagate panic overrides for utest through dependencies

macro_rules! eprint {
    ($($tt:tt)*) => {};
}
macro_rules! eprintln {
    ($($tt:tt)*) => {};
}

#[no_mangle]
pub fn __test_start(ntests: usize) {
    // take_root_caps()
    // configure everything
    // unsafe { untake_root_caps() }
    eprintln!("running {} tests", ntests);
}

#[no_mangle]
pub fn __test_ignored(name: &str) {
    eprintln!("test {} ... ignored", name);
}

#[no_mangle]
pub fn __test_before_run(name: &str) {
    eprint!("test {} ... ", name);
}

#[no_mangle]
pub fn __test_panic_fmt(args: ::core::fmt::Arguments, file: &'static str, line: u32) {
    eprint!("\npanicked at '");
    eprintln!("', {}:{}", file, line);
}

#[no_mangle]
pub fn __test_failed(_name: &str) {
    eprintln!("FAILED");
}

#[no_mangle]
pub fn __test_success(_name: &str) {
    eprintln!("OK");
}

#[no_mangle]
pub fn __test_summary(passed: usize, failed: usize, ignored: usize) {
    eprintln!(
        "\ntest result: {}. {} passed; {} failed; {} ignored",
        if failed == 0 { "OK" } else { "FAILED" },
        passed,
        failed,
        ignored
    );

    unsafe {
        core::intrinsics::abort();
    }
}
