#define HAVE_AUTOCONF
// End arch dependent

#include "arch/types.h"
#include "sel4/sel4_arch/simple_types.h"
#include "sel4/sel4_arch/types.h"
#include "api/types.h"
#include "sel4/macros.h"
#include "sel4/constants.h"
#include "sel4/sel4_arch/constants.h"
#include "sel4/shared_types.h"
