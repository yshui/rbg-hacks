use crate::*;

// 10.1.0 does not use mcs-style reply
//
// A reply should borrow the endpoint. Conveniently no mutability is necessary, so there may exist
// many reply-handles.
impl Endpoint {}
