use crate::Error;
use core::marker::PhantomData;
use core::mem::{size_of, align_of};

/// Used as container for segmented memory access
pub struct AddressSpace<T>(PhantomData<T>);

/// The GS segment
pub struct GS(!);

pub struct ASPtr<AS, T> {
    offset: usize,
    _marker: (PhantomData<T>, PhantomData<AS>),
}

pub struct ASSlice<AS, T> {
    offset: usize,
    len: usize,
    _marker: (PhantomData<T>, PhantomData<AS>),
}

impl<AS> AddressSpace<AS> {
    /// Get a pointer in the given address space.
    /// Returns None when size_of::<T> is not a multiple of size_of::<usize>
    #[inline]
    pub fn ptr<T>(offset: usize) -> Option<ASPtr<AS, T>> {
        if size_of::<T>() % size_of::<usize>() != 0 ||
           align_of::<T>() != align_of::<usize>() {
            None
        } else {
            Some(ASPtr::<AS, T> {
                offset,
                _marker: (PhantomData::<T>, PhantomData::<AS>),
            })
        }
    }

    /// Get a slice in the given address space
    /// Same constraint as ptr() applies here
    #[inline]
    pub fn slice<T>(offset: usize, len: usize) -> Option<ASSlice<AS, T>> {
        if size_of::<T>() % size_of::<usize>() != 0  ||
           align_of::<T>() != align_of::<usize>() {
            None
        } else {
            Some(ASSlice::<AS, T> {
                offset,
                len,
                _marker: (PhantomData::<T>, PhantomData::<AS>),
            })
        }
    }
}

pub trait ASAccess<T> {
    unsafe fn read(&self) -> T;
    unsafe fn write(&self, input: &T);
}

impl<T> ASAccess<T> for ASPtr<GS, T> {
    #[inline]
    unsafe fn read(&self) -> T {
        // FIXME: use LLVM-IR for optimized @llvm.memcpy between address spaces 256 and 0
        let mut ret = core::mem::uninitialized();
        let buf = core::slice::from_raw_parts_mut(
            &mut ret as *mut _,
            size_of::<T>() / size_of::<usize>(),
        );
        const SIZE: usize = size_of::<usize>();
        for (i, word) in buf.iter_mut().enumerate() {
            asm!("movq %gs:0($1, $2, ${3:c}), $0"
                 : "=r"(*word)
                 : "r"(self.offset), "ri"(i), "i"(SIZE)
                 :
                 : "volatile"
            );
        }
        ret
    }

    #[inline]
    unsafe fn write(&self, input: &T) {
        // FIXME: use LLVM-IR for optimized @llvm.memcpy between address spaces 0 and 256
        let buf =
            core::slice::from_raw_parts(&input as *const _, size_of::<T>() / size_of::<usize>());
        const SIZE: usize = size_of::<usize>();
        for (i, word) in buf.iter().enumerate() {
            asm!("movq $0, %gs:0($1, $2, ${3:c})"
             :
             : "ri"(*word), "r"(self.offset), "ri"(i), "i"(SIZE)
             : "memory"
             : "volatile"
            );
        }
    }
}

impl<AS, T> ASSlice<AS, T>
where
    ASPtr<AS, T>: ASAccess<T>,
{
    #[inline]
    pub fn ptr_at(&self, offset: usize) -> ASPtr<AS, T> {
        ASPtr::<AS, T> {
            offset: self.offset + size_of::<T>() * offset,
            _marker: (PhantomData::<T>, PhantomData::<AS>),
        }
    }
    #[inline]
    pub unsafe fn read(&self, output: &mut [T]) -> Result<(), Error> {
        if output.len() > self.len {
            Err(Error::RangeError)
        } else {
            for (i, o) in output.iter_mut().enumerate() {
                *o = self.ptr_at(i).read()
            }
            Ok(())
        }
    }
    #[inline]
    pub unsafe fn write(&self, input: &[T]) -> Result<(), Error> {
        if input.len() > self.len {
            Err(Error::RangeError)
        } else {
            for (i, v) in input.iter().enumerate() {
                self.ptr_at(i).write(v)
            }
            Ok(())
        }
    }
}
