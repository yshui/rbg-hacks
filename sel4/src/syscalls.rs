#![allow(dead_code, clippy::too_many_arguments)]

use crate::CPtr;

// TODO: generate these...
#[repr(isize)]
#[derive(Debug)]
pub enum Syscall {
    Call = -1,
    ReplyRecv = -2,
    NBSendRecv = -3,
    NBSendWait = -4,
    Send = -5,
    NBSend = -6,
    Recv = -7,
    NBRecv = -8,
    Wait = -9,
    NBWait = -10,
    Yield = -11,
    DebugPutChar = -12,
    DebugHalt = -13,
    DebugCapIdentify = -14,
    DebugSnapshot = -15,
    DebugNameThread = -16,
    DebugRun = -17,
    BenchmarkFlushCaches = -18,
    BenchmarkResetLog = -19,
    BenchmarkFinalizeLog = -20,
    BenchmarkSetLogBuffer = -21,
    BenchmarkNullSyscall = -22,
    BenchmarkGetThreadUtilisation = -23,
    BenchmarkResetThreadUtilisation = -24,
    VMEnter = -25,
}

#[inline(always)]
pub(crate) fn x64_sys_send(
    sys: Syscall,
    dest: CPtr,
    info: usize,
    mr1: usize,
    mr2: usize,
    mr3: usize,
    mr4: usize,
) {
    unsafe {
        asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              :
              : "{rdx}" (sys as usize),
                "{rdi}" (dest.0),
                "{rsi}" (info),
                "{r10}" (mr1),
                "{r8}" (mr2),
                "{r9}" (mr3),
                "{r15}" (mr4)
              : "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_send_null(sys: Syscall, dest: CPtr, info: usize) {
    unsafe {
        asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              :
              : "{rdx}" (sys as usize),
                "{rdi}" (dest.0),
                "{rsi}" (info)
              : "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_recv(
    sys: Syscall,
    src: CPtr,
    out_badge: &mut usize,
    out_info: &mut usize,
    out_mr1: &mut usize,
    out_mr2: &mut usize,
    out_mr3: &mut usize,
    out_mr4: &mut usize,
    reply: usize,
) {
    unsafe {
        asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              : "={rsi}" (*out_info),
                "={r10}" (*out_mr1),
                "={r8}" (*out_mr2),
                "={r9}" (*out_mr3),
                "={r15}" (*out_mr4),
                "={rdi}" (*out_badge)
              : "{rdx}" (sys as usize),
                "{rdi}" (src.0),
                "{r12}" (reply)
              : "memory", "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_send_recv(
    sys: Syscall,
    dest: CPtr,
    out_dest: &mut CPtr,
    info: usize,
    out_info: &mut usize,
    in_out_mr1: &mut usize,
    in_out_mr2: &mut usize,
    in_out_mr3: &mut usize,
    in_out_mr4: &mut usize,
    reply: usize,
) {
    unsafe {
        asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              : "={rsi}" (*out_info),
                "={r10}" (*in_out_mr1),
                "={r8}" (*in_out_mr2),
                "={r9}" (*in_out_mr3),
                "={r15}" (*in_out_mr4),
                "={rdi}" (out_dest.0)
              : "{rdx}" (sys as usize),
                "{rsi}" (info),
                "{r10}" (*in_out_mr1),
                "{r8}" (*in_out_mr2),
                "{r9}" (*in_out_mr3),
                "{r15}" (*in_out_mr4),
                "{r12}" (reply),
                "{rdi}" (dest.0)
              : "memory", "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_nbsend_recv(
    sys: Syscall,
    dest: CPtr,
    src: CPtr,
    out_dest: &mut CPtr,
    info: usize,
    out_info: &mut usize,
    in_out_mr1: &mut usize,
    in_out_mr2: &mut usize,
    in_out_mr3: &mut usize,
    in_out_mr4: &mut usize,
    reply: usize,
) {
    unsafe {
        asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              : "={rsi}" (*out_info),
                "={r10}" (*in_out_mr1),
                "={r8}" (*in_out_mr2),
                "={r9}" (*in_out_mr3),
                "={r15}" (*in_out_mr4),
                "={rdi}" (out_dest.0)
              : "{rdx}" (sys as usize),
                "{rdi}" (src.0),
                "{rsi}" (info),
                "{r10}" (*in_out_mr1),
                "{r8}" (*in_out_mr2),
                "{r9}" (*in_out_mr3),
                "{r15}" (*in_out_mr4),
                "{r12}" (reply),
                "{r13}" (dest.0)
              : "memory", "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_null(sys: Syscall) {
    unsafe {
        asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              :
              : "{rdx}" (sys as usize)
              : "rcx", "rbx", "r11", "rsi", "rdi"
              : "volatile");
    }
}
