use core::mem;

use crate::generated::seL4_CPtr;
use crate::{Error, Time};

/// An unowned capability pointer. Use `Cap` for owned handles.
#[derive(Clone, Copy)]
pub struct CPtr(pub(crate) seL4_CPtr);
/// An owned CPtr. Unsafe to construct and safe to invoke.
pub struct Cap(CPtr);

pub struct CSlot {
    pub(crate) cptr: CPtr,
    pub(crate) depth: u8,
}

impl CPtr {
    pub fn null() -> CPtr {
        CPtr(0)
    }
}

impl CSlot {
    pub fn null() -> CSlot {
        CSlot {
            cptr: CPtr::null(),
            depth: 0,
        }
    }
}

impl Cap {
    pub unsafe fn from_cptr(cptr: CPtr) -> Self {
        Cap(cptr)
    }

    pub fn as_cptr(&self) -> CPtr {
        self.0
    }

    // blocking
    // send + recv
    #[inline]
    pub fn call(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    // send_nb + recv
    #[inline]
    pub fn reply_recv(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn send(&self, _label: usize, _data_in: &[usize], _cap_in: &[CPtr]) -> Result<(), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn recv(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn wait(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    // non-blocking
    #[inline]
    pub fn send_nb(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
    ) -> Result<(), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn recv_nb(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn send_recv_nb(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn send_wait_nb(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn wait_nb(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }
}

pub(crate) trait ReaderWriter {
    const DATA_SIZE: usize;
    const CAPS_SIZE: usize;

    fn read(words: &mut &[usize], caps: &mut &[CPtr]) -> Self;
    fn write(&self, words: &mut &mut [usize], caps: &mut &mut [CPtr]);
}

impl<T> ReaderWriter for T
where
    T: AsRef<Cap> + From<Cap>,
{
    const DATA_SIZE: usize = 0;
    const CAPS_SIZE: usize = 1;

    #[inline]
    fn write(&self, _words: &mut &mut [usize], caps: &mut &mut [CPtr]) {
        let (l, r) = mem::replace(caps, &mut []).split_at_mut(Self::CAPS_SIZE);
        *caps = r;
        l[0] = self.as_ref().as_cptr();
    }

    #[inline]
    fn read(_words: &mut &[usize], caps: &mut &[CPtr]) -> Self {
        let (l, r) = mem::replace(caps, &[]).split_at(Self::CAPS_SIZE);
        *caps = r;
        T::from(Cap(l[0]))
    }
}

impl ReaderWriter for CPtr {
    const DATA_SIZE: usize = 0;
    const CAPS_SIZE: usize = 1;

    #[inline]
    fn write(&self, _words: &mut &mut [usize], caps: &mut &mut [CPtr]) {
        let (l, r) = mem::replace(caps, &mut []).split_at_mut(Self::CAPS_SIZE);
        *caps = r;
        l[0] = *self;
    }

    #[inline]
    fn read(_words: &mut &[usize], caps: &mut &[CPtr]) -> Self {
        let (l, r) = mem::replace(caps, &[]).split_at(Self::CAPS_SIZE);
        *caps = r;
        l[0]
    }
}

impl ReaderWriter for Cap {
    const DATA_SIZE: usize = 0;
    const CAPS_SIZE: usize = 1;

    #[inline]
    fn write(&self, _words: &mut &mut [usize], caps: &mut &mut [CPtr]) {
        let (l, r) = mem::replace(caps, &mut []).split_at_mut(Self::CAPS_SIZE);
        *caps = r;
        l[0] = self.as_cptr();
    }

    #[inline]
    fn read(_words: &mut &[usize], caps: &mut &[CPtr]) -> Self {
        let (l, r) = mem::replace(caps, &[]).split_at(Self::CAPS_SIZE);
        *caps = r;
        unsafe { Cap::from_cptr(l[0]) }
    }
}

impl ReaderWriter for Time {
    const DATA_SIZE: usize = 1;
    const CAPS_SIZE: usize = 0;

    #[inline]
    fn write(&self, words: &mut &mut [usize], _caps: &mut &mut [CPtr]) {
        let (l, r) = mem::replace(words, &mut []).split_at_mut(Self::DATA_SIZE);
        *words = r;
        l[0] = self.0 as usize;
    }

    #[inline]
    fn read(words: &mut &[usize], _caps: &mut &[CPtr]) -> Self {
        let (l, r) = mem::replace(words, &[]).split_at(Self::DATA_SIZE);
        *words = r;
        Time(l[0] as u64)
    }
}

impl ReaderWriter for usize {
    const DATA_SIZE: usize = 1;
    const CAPS_SIZE: usize = 0;

    #[inline]
    fn write(&self, words: &mut &mut [usize], _caps: &mut &mut [CPtr]) {
        let (l, r) = mem::replace(words, &mut []).split_at_mut(Self::DATA_SIZE);
        *words = r;
        l[0] = *self;
    }

    #[inline]
    fn read(words: &mut &[usize], _caps: &mut &[CPtr]) -> Self {
        let (l, r) = mem::replace(words, &[]).split_at(Self::DATA_SIZE);
        *words = r;
        l[0]
    }
}

impl ReaderWriter for bool {
    const DATA_SIZE: usize = 1;
    const CAPS_SIZE: usize = 0;

    #[inline]
    fn write(&self, words: &mut &mut [usize], _caps: &mut &mut [CPtr]) {
        let (l, r) = mem::replace(words, &mut []).split_at_mut(Self::DATA_SIZE);
        *words = r;
        l[0] = *self as usize;
    }

    #[inline]
    fn read(words: &mut &[usize], _caps: &mut &[CPtr]) -> Self {
        let (l, r) = mem::replace(words, &[]).split_at(Self::DATA_SIZE);
        *words = r;
        l[0] == 1
    }
}

macro_rules! simple {
    ($($tt:tt)*) => {
        $(
            impl ReaderWriter for $tt {
                const DATA_SIZE: usize = 1;
                const CAPS_SIZE: usize = 0;

                #[inline]
                fn write(&self, words: &mut &mut [usize], _caps: &mut &mut [CPtr]) {
                    let (l, r) = mem::replace(words, &mut [])
                        .split_at_mut(Self::DATA_SIZE);
                    *words = r;
                    l[0] = *self as usize;
                }

                #[inline]
                fn read(words: &mut &[usize], _caps: &mut &[CPtr]) -> Self {
                    let (l, r) = mem::replace(words, &[])
                        .split_at(Self::DATA_SIZE);
                    *words = r;
                    l[0] as $tt
                }
            }
        )*
    }
}

simple!(isize u8 i8 u16 i16 u32 i32 u64 i64);
