use std::collections::HashMap;
use std::{env, fs, io, path, process};
use std::{fmt::Write, io::Write as IoWrite};

use roxmltree::Document;
use string_morph::Morph;
use toml::Value as TomlValue;

// FIXME: understand conditionals? #if / #else / #elseif
//
// Alternatively, these should be defined in the sel4{,arch}.xml files:
//
// * arch_include/x86/sel4/arch/constants.h
// * sel4_arch_include/x86_64/sel4/sel4_arch/constants.h
// * sel4_plat_include/pc99/sel4/plat/api/constants.h
//
// FIXME: support more architectures
//
// FIXME: detect changes in sel4/libsel4/tools/sel4_idl.dtd
//
// FIXME: handle bitfield files?

macro_rules! attr {
    ($attr:tt $key:tt $ty:tt $case:tt) => {{
        let tmp = $attr
            .attribute($key)
            .expect(concat!(stringify!($attr), " expected field: ", $key))
            .trim_start_matches("seL4")
            .$case();
        match &*tmp {
            // HACKS: use a complete list of rust keywords and types
            "move" => "move_",
            "type" => "type_",
            "Int" => "isize",
            "int8" => "i8",
            "int16" => "i16",
            "int32" => "i32",
            "int64" => "i64",
            "UInt" => "usize",
            "Uint8" => "u8",
            "Uint16" => "u16",
            "Uint32" => "u32",
            "Uint64" => "u64",
            "Bool" => "bool",
            // seL4_Word is always usize
            "Word" => "usize",
            // Could catch the last character being T?
            "CapRightsT" => "CapRights",
            // Correct case for all UPPERCASE things
            "Tcb" => "TCB",
            // x64
            "X64PmL4" => "X64PML4",
            "X64Pdpt" => "X64PDPT",
            // x86
            "X86Eptpd" => "X86EPTPD",
            "X86Eptpt" => "X86EPTPT",
            "X86Eptpdpt" => "X86EPTPDPT",
            "X86EptpmL4" => "X86EPTPML4",
            "X86Vcpu" => "X86VCPU",
            _ => &tmp,
        }
        .to_string()
    }};
}

fn main() {
    println!("cargo:rerun-if-changed=src/wrapper.h");

    let cargo_manifest_dir = path::PathBuf::from(
        env::var("CARGO_MANIFEST_DIR").expect("Expected CARGO_MANIFEST_DIR, are you using cargo?"),
    );
    println!("cargo:rerun-if-env-changed=OUT_DIR");
    let outdir = path::PathBuf::from(env::var("OUT_DIR").expect("expected OUT_DIR"));

    let mut cmake_conf = cmake::Config::new("sel4");
    cmake_conf
        .generator("Ninja")
        .build_target("kernel.elf")
        .always_configure(true);

    println!("cargo:rerun-if-env-changed=ROBIGALIA_KERNEL_CONFIG");
    let kernel_config = env::var("ROBIGALIA_KERNEL_CONFIG")
        .map(path::PathBuf::from)
        .unwrap_or(cargo_manifest_dir.join("robigalia-kernel-config.toml"));
    println!("cargo:rerun-if-changed={}", kernel_config.display());

    if let TomlValue::Table(table) = fs::read_to_string(kernel_config)
        .expect("failed to read kernel configuration file")
        .parse::<TomlValue>()
        .expect("failed to parse kernel config to toml")
    {
        for (target, table) in table {
            match &*target {
                "x86_64-sel4-robigalia" => (),
                _ => panic!("unknown target: {}", target),
            }
            if let TomlValue::Table(table) = table {
                let mut tables = vec![table];
                while let Some(table) = tables.pop() {
                    for (key, value) in table {
                        match value {
                            TomlValue::String(a) => {
                                cmake_conf.define(key, a);
                            }
                            TomlValue::Integer(a) => {
                                cmake_conf.define(key, a.to_string());
                            }
                            TomlValue::Boolean(true) => {
                                cmake_conf.define(key, "ON");
                            }
                            TomlValue::Boolean(false) => {
                                cmake_conf.define(key, "OFF");
                            }
                            TomlValue::Table(mut t) => {
                                if key == "debug" {
                                    tables.push(core::mem::replace(&mut t, Default::default()))
                                } else {
                                    panic!("unknown key: {}", key)
                                }
                            }
                            _ => panic!("key has invalid value: {}", key),
                        }
                    }
                }
            } else {
                panic!("expected table!");
            }
        }
    } else {
        panic!("expected table!");
    }

    assert_eq!(cmake_conf.build(), outdir);

    let mut out = io::BufWriter::new(
        fs::File::create(outdir.join("generated.rs"))
            .expect("failed to open file out/generated.rs"),
    );

    let sel4_dir = cargo_manifest_dir.join("sel4");
    let sel4_arch_inc = sel4_dir.join("include").join("arch");
    let libsel4_dir = sel4_dir.join("libsel4");
    let build = outdir.join("build");
    let libsel4_gen = build.join("libsel4");
    let binding = bindgen::Builder::default()
        .header("src/wrapper.h")
        .ctypes_prefix("c_types")
        .ignore_functions()
        .whitelist_type("seL4_MessageInfo_t")
        .whitelist_type("seL4_IPCBuffer")
        .clang_arg(format!("-I{}", sel4_dir.join("include").display()))
        .clang_arg(format!("-I{}", sel4_arch_inc.join("x86").display()))
        .clang_arg(format!(
            "-I{}",
            sel4_arch_inc.join("x86").join("arch").join("64").display()
        ))
        .clang_arg(format!(
            "-I{}",
            sel4_dir.join("include").join("64").display()
        ))
        .clang_arg(format!("-I{}", libsel4_dir.join("include").display()))
        .clang_arg(format!(
            "-I{}",
            libsel4_dir.join("arch_include").join("x86").display()
        ))
        .clang_arg(format!(
            "-I{}",
            libsel4_dir
                .join("sel4_arch_include")
                .join("x86_64")
                .display()
        ))
        .clang_arg(format!("-I{}", build.join("autoconf").display()))
        .clang_arg(format!("-I{}", build.join("gen_config").display()))
        .clang_arg(format!("-I{}", build.join("generated").display()))
        .generate()
        .expect("failed to generate binding")
        .to_string();

    out.write(binding.as_bytes())
        .expect("failed to write out binding");

    fs::copy(
        outdir.join("build").join("kernel.elf"),
        cargo_manifest_dir.join("kernel.elf"),
    )
    .expect("failed to copy kernel");

    let kernel_config_src: String = fs::read_to_string(
        outdir
            .join("build")
            .join("gen_config")
            .join("kernel")
            .join("gen_config.h"),
    )
    .expect("failed to read gen_config.h");

    for line in kernel_config_src
        .lines()
        .map(|l| l.trim_start_matches("#define "))
    {
        let space = line.find(" ").expect("failed to find space!");
        let name = &line[..space];
        let value = &line[space + 1..];

        println!(r#"cargo:rustc-cfg=feature="{}={}""#, name, value);
        if let Ok(int) = value.parse::<usize>() {
            writeln!(out, "pub const {}: usize = {};", name, value).unwrap();

            // Probably 1 = true. If not, oh well.
            if int == 1 {
                println!(r#"cargo:rustc-cfg=feature="{}""#, name);
                writeln!(out, r#"// FEATURE: {}"#, name).unwrap();
            }

            // Add special cases for any integer comparisons.
            //
            // FIXME: verify the generated feature flags match the expectations of the
            // sel4{,arch}.xml specifications.
            //
            // * CONFIG_VTX
            // * CONFIG_ENABLE_BENCHMARKS
            // * more...
            match name {
                "CONFIG_MAX_NUM_NODES" if int > 0 => {
                    println!(r#"cargo:rustc-cfg=feature="CONFIGX_ENABLE_SMP_SUPPORT""#);
                    writeln!(out, r#"// FEATURE: CONFIGX_ENABLE_SMP_SUPPORT"#).unwrap();
                }
                _ => (),
            }
        } else {
            writeln!(out, r#"pub const {}: &str = "{}";"#, name, value).unwrap();
        }
    }

    // FIXME: support more than just x86_64!
    let files = vec![
        cargo_manifest_dir
            .join("sel4")
            .join("libsel4")
            .join("include")
            .join("interfaces")
            .join("sel4.xml"),
        cargo_manifest_dir
            .join("sel4")
            .join("libsel4")
            .join("sel4_arch_include")
            .join("x86_64")
            .join("interfaces")
            .join("sel4arch.xml"),
        cargo_manifest_dir
            .join("sel4")
            .join("libsel4")
            .join("arch_include")
            .join("x86")
            .join("interfaces")
            .join("sel4arch.xml"),
    ];

    let mut overrides = Vec::new();
    let mut renames = HashMap::new();
    let idiomatic = fs::read_to_string(cargo_manifest_dir.join("src").join("lib.rs"))
        .expect("failed to read src/lib.rs");
    for line in idiomatic.lines().map(|l| l.trim()) {
        if line.starts_with("// OVERRIDE: ") {
            overrides.push(line.trim_start_matches("// OVERRIDE: "));
        } else if line.starts_with("// RENAME: ") {
            let tmp = line.trim_start_matches("// RENAME: ");
            let (from, to) = tmp.split_at(tmp.find("=").unwrap());
            renames.insert(from.trim(), to[1..].trim());
        }
    }

    let mut invocation_labels = String::new();

    for f in files {
        let xml_source = fs::read_to_string(f).expect("failed to read file to string");

        let doc = Document::parse(&xml_source).expect("failed to parse document");
        let root = doc.root();
        let api = root
            .children()
            .filter(|el| el.is_element())
            .next()
            .expect("API element not found!");
        assert_eq!(api.tag_name().name(), "api", "Unexpected element!");

        let module_name = attr!(api "name" Ident to_snake_case);

        // interfaces
        for api_child in api.children().filter(|el| el.is_element()) {
            match api_child.tag_name().name() {
                "struct" => {
                    let struct_name = attr!(api_child "name" Ident to_pascal_case);
                    let mut fields = String::new();
                    let mut write = String::new();
                    let mut read = String::new();
                    let mut count = 0usize;
                    for field in api_child.children().filter(|el| el.is_element()) {
                        assert_eq!(field.tag_name().name(), "member", "Unexpected element!");
                        let name = attr!(field "name" Ident to_snake_case);
                        writeln!(fields, "{}: usize,", name).unwrap();
                        writeln!(write, "l[{}] = self.{};", count, name).unwrap();
                        writeln!(read, "{}: l[{}],", name, count).unwrap();
                        count += 1;
                    }
                    writeln!(
                        out,
                        "
                            pub struct {struct_name} {{
                                {fields}
                            }}

                            impl ReaderWriter for {struct_name} {{
                                const DATA_SIZE: usize = {count};
                                const CAPS_SIZE: usize = 0;

                                #[inline]
                                fn write(&self, words: &mut &mut [usize], _caps: &mut &mut [CPtr]) {{
                                    let (l, r) = mem::replace(words, &mut [])
                                        .split_at_mut(Self::DATA_SIZE);
                                    *words = r;
                                    {write}
                                }}

                                #[inline]
                                fn read(words: &mut &[usize], _caps: &mut &[CPtr]) -> Self {{
                                    let (l, r) = mem::replace(words, &[])
                                        .split_at(Self::DATA_SIZE);
                                    *words = r;
                                    Self {{
                                        {read}
                                    }}
                                }}
                            }}
                        ",
                        struct_name = struct_name,
                        fields = fields,
                        write = write,
                        read = read,
                        count = count,
                    ).unwrap();
                }
                "interface" => {
                    let struct_name = attr!(api_child "name" Ident to_pascal_case);
                    let mut methods = String::new();
                    // methods
                    for method in api_child.children().filter(|el| el.is_element()) {
                        assert_eq!(method.tag_name().name(), "method", "Unexpected element!");
                        let mut method_name = attr!(method "name" Ident to_snake_case).to_string();
                        let invocation_label =
                            format!("{}_{}", struct_name, method_name).to_pascal_case();
                        let method_path = format!("{}::{}", struct_name, method_name,);
                        // rename functions
                        if let Some(new_name) = renames.get(&&*method_path) {
                            method_name = new_name.to_string();
                        };
                        // make overriden functions private and add the _internal suffix
                        let vis = if overrides.contains(&&*method_path) {
                            method_name = format!("{}_internal", method_name);
                            "pub(crate)"
                        } else {
                            "pub"
                        };
                        let condition = if let Some(c) = method.attribute("condition") {
                            let c = match c {
                                "CONFIG_MAX_NUM_NODES > 1" => "CONFIGX_ENABLE_SMP_SUPPORT",
                                _ if c.starts_with("defined(") => {
                                    c.trim_start_matches("defined(").trim_end_matches(")")
                                }
                                c => panic!("unhandled condition: {}", c),
                            };
                            format!(r#"#[cfg(any(feature = "{}", rustdoc))]"#, c)
                        } else {
                            String::with_capacity(0)
                        };
                        writeln!(invocation_labels, "{} {},", condition, invocation_label).unwrap();
                        let mut data_in_len = "0usize".to_string();
                        let mut caps_in_len = "0usize".to_string();
                        let mut data_out_len = "0usize".to_string();
                        let mut caps_out_len = "0usize".to_string();
                        let mut input_params = String::new();
                        let mut output_params = String::new();
                        let mut encode_input = String::new();
                        let mut decode_output = String::new();
                        // method properties
                        for method_child in method.children().filter(|el| el.is_element()) {
                            match method_child.tag_name().name() {
                                // FIXME: parse documentation from sel4{,arch}.xml
                                "brief" | "description" | "return" => (),
                                "cap_param" => (),
                                "param" => {
                                    let name = attr!(method_child "name" Ident to_snake_case);
                                    let type_ = attr!(method_child "type" Ident to_pascal_case);
                                    // FIXME: collect a list of capability-types to borrow them for
                                    // generated methods
                                    match method_child
                                        .attribute("dir")
                                        .expect("el must have attribute dir!")
                                    {
                                        "in" => {
                                            writeln!(data_in_len, "+ {}::DATA_SIZE", type_)
                                                .unwrap();
                                            writeln!(caps_in_len, "+ {}::CAPS_SIZE", type_)
                                                .unwrap();
                                            writeln!(
                                                encode_input,
                                                "{}.write(data_cursor, caps_cursor);",
                                                name
                                            )
                                            .unwrap();
                                            writeln!(input_params, "{}: {},", name, type_).unwrap();
                                        }
                                        "out" => {
                                            writeln!(data_out_len, "+ {}::DATA_SIZE", type_)
                                                .unwrap();
                                            writeln!(caps_out_len, "+ {}::CAPS_SIZE", type_)
                                                .unwrap();
                                            writeln!(
                                                decode_output,
                                                "{}::read(data_cursor, caps_cursor),",
                                                type_
                                            )
                                            .unwrap();
                                            writeln!(output_params, "{},", type_).unwrap();
                                        }
                                        dir => panic!("unhandled: {}", dir),
                                    }
                                }
                                name => panic!("unhandled: {}", name),
                            }
                        }
                        writeln!(
                            methods,
                            "
                            {condition}
                            #[inline]
                            #[allow(unused_variables)]
                            {vis} fn {method_name}(&self, {input_params})
                                -> Result<({output_params}), Error>
                            {{
                                let mut data_in = [0usize; {data_in_len}];
                                let mut caps_in = [CPtr(0usize); {caps_in_len}];
                                {{
                                    let data_cursor = &mut &mut data_in[..];
                                    let caps_cursor = &mut &mut caps_in[..];
                                    {encode_input}
                                }}
                                let mut data_out = [0usize; {data_out_len}];
                                let mut caps_out = [CPtr(0usize); {caps_out_len}];
                                self.0.call(
                                        InvocationLabel::{invocation_label} as usize,
                                        &data_in[..],
                                        &caps_in[..],
                                        &mut data_out[..],
                                        &mut caps_out[..],
                                    )
                                    .map(|(data_out, caps_out)| {{
                                        let data_cursor = &mut &data_out[..];
                                        let caps_cursor = &mut &caps_out[..];
                                        ({decode_output})
                                    }})
                            }}
                            ",
                            condition = condition,
                            method_name = method_name,
                            vis = vis,
                            encode_input = encode_input,
                            data_out_len = data_out_len.trim_start_matches("0usize+ "),
                            caps_out_len = caps_out_len.trim_start_matches("0usize+ "),
                            data_in_len = data_in_len.trim_start_matches("0usize+ "),
                            caps_in_len = caps_in_len.trim_start_matches("0usize+ "),
                            invocation_label = invocation_label,
                            input_params = input_params,
                            output_params = output_params.trim().trim_end_matches(","),
                            decode_output = decode_output.trim().trim_end_matches(","),
                        )
                        .unwrap();
                    }
                    // FIXME: should detect collisions naturally and reuse the object
                    if !(module_name != "object_api"
                        && (struct_name == "IRQControl" || struct_name == "TCB"))
                    {
                        writeln!(
                            out,
                            "
                                pub struct {0}(crate::Cap);

                                impl AsRef<Cap> for {0} {{
                                    fn as_ref(&self) -> &Cap {{
                                        &self.0
                                    }}
                                }}

                                impl From<Cap> for {0} {{
                                    fn from(cptr: Cap) -> Self {{
                                        {0}(cptr)
                                    }}
                                }}
                            ",
                            struct_name
                        )
                        .unwrap();
                    }
                    writeln!(out, "impl {} {{ {} }}", struct_name, methods.to_string()).unwrap();
                }
                e => panic!("unhandled! {}", e),
            }
        }
    }
    writeln!(
        out,
        "#[repr(usize)] enum InvocationLabel {{ {} }}",
        invocation_labels
    )
    .unwrap();

    let xml_source = fs::read_to_string(
        cargo_manifest_dir
            .join("sel4")
            .join("libsel4")
            .join("include")
            .join("api")
            .join("syscall.xml"),
    )
    .expect("failed to read file to string");
    let doc = Document::parse(&xml_source).expect("failed to parse document");
    let root = doc.root_element();

    writeln!(out, "#[repr(usize)] enum Syscalls {{").unwrap();
    for group in root.children().filter(|el| el.is_element()) {
        for config in group.children().filter(|el| el.is_element()) {
            let condition = if let Some(c) = config.attribute("condition") {
                let c = match c {
                    _ if c.starts_with("defined ") => c.trim_start_matches("defined "),
                    c => panic!("unhandled condition: {}", c),
                };
                format!(r#"#[cfg(any(feature = "{}", rustdoc))]"#, c)
            } else {
                String::with_capacity(0)
            };
            for syscall in config.children().filter(|el| el.is_element()) {
                let name = attr!(syscall "name" Ident to_pascal_case);
                writeln!(out, "{} {},", condition, name).unwrap();
            }
        }
    }
    writeln!(out, "}}").unwrap();

    drop(out);

    process::Command::new("rustfmt")
        .arg("generated.rs")
        .current_dir(outdir)
        .output()
        .expect("failed to format generated.rs");
}
